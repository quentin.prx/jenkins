package ws;

import java.sql.SQLException;
import java.util.List;

import javax.jws.*;
import entities.Product;

@WebService
public interface ProductInterface {
	
	@WebMethod
	public Product findById(int id);
	@WebMethod 
	public List<Product> findAll();
	
	@WebMethod
	public Product findByIdSQL(int id) throws ClassNotFoundException, SQLException;
	
	@WebMethod
	public List<Product> findAllSQL() throws ClassNotFoundException, SQLException;
}
