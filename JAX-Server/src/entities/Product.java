package entities;

public class Product {
	private int id;
	private String libelle;
	private double prix;
	private String description;
	
	public Product(int id, String libelle, double prix, String description) {
		this.id = id;
		this.libelle = libelle;
		this.prix = prix;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String toString() {
		return "Product [id=" + id + ", libelle=" + libelle + ", prix=" + prix + ", description=" + description + "]";
	}	
	
}
