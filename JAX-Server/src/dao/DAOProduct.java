package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.Product;

public class DAOProduct {
	public List<Product> products = new ArrayList<Product>(){{
															add(new Product(1, "Voiture", 40000.0, "Renaud ZOE"));
															add(new Product(2,"V�lo",950.99,"V�lo de course"));
															add(new Product(3,"Train",20000.0,"Train OUIGO"));
															add(new Product(4,"Avion",3000000.0,"Airbus"));
															}};
	
	public Product findById(int id) {
		Product prod = null;
		for(Product p : products) {
			if(p.getId() == id)
				return p;
		}
		return prod;
	}
	
	public List<Product> findAll() {
		return products;
	}
	
	
	public Product findByIdSQL(int id) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/Produit", "root", "" );
		
		String sql = "SELECT * FROM product where id=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		Product p=null;
		while(rs.next()) {
			p = new Product(rs.getInt("id"), rs.getString("libelle"), rs.getDouble("prix"), rs.getString("description"));
		}
		
		
		return p;
		
	}
	
	public List<Product> findAllSQL() throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/Produit", "root", "" );
		
		String sql = "SELECT * FROM product";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		List<Product> prods = new ArrayList<Product>();
		while(rs.next()) {
			prods.add(new Product(rs.getInt("id"), rs.getString("libelle"), rs.getDouble("prix"), rs.getString("description")));
		}
		
		rs.close();
		ps.close();
		conn.close();
		return prods;
	}
}
